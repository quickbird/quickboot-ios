//
//  KeyboardObserver.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

public class KeyboardObserver {

    public struct KeyboardChange {
        public let frame: CGRect
        public let animationDuration: TimeInterval
    }

    private static let keyboardWillShowSignal = NotificationCenter.default.rx.notification(UIResponder.keyboardWillShowNotification)
    private static let keyboardWillHideSignal = NotificationCenter.default.rx.notification(UIResponder.keyboardWillHideNotification)

    public static var keyboardFrame: Observable<CGRect> {
        let keyboardWillShowFrames = keyboardWillShowSignal.map { notification -> CGRect in
            guard let userInfo = notification.userInfo,
                let keyboardFrameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue else { return .zero }
            return keyboardFrameValue.cgRectValue
        }

        let keyboardWillHideFrames = keyboardWillHideSignal.map { _ -> CGRect in
            return CGRect.zero
        }

        return Observable.merge(keyboardWillShowFrames, keyboardWillHideFrames)
    }

    public static var keyboardChange: Observable<KeyboardChange> {
        let keyboardWillShowFrames = keyboardWillShowSignal.map { notification -> KeyboardChange in
            guard let userInfo = notification.userInfo,
                let keyboardFrameValue = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue,
                let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval else { return KeyboardChange(frame: .zero, animationDuration: 0.0) }
            return KeyboardChange(frame: keyboardFrameValue.cgRectValue, animationDuration: animationDuration)
        }

        let keyboardWillHideFrames = keyboardWillHideSignal.map { notification -> KeyboardChange in
            guard let userInfo = notification.userInfo,
                let animationDuration = userInfo[UIResponder.keyboardAnimationDurationUserInfoKey] as? TimeInterval else { return KeyboardChange(frame: .zero, animationDuration: 0.0) }
            return KeyboardChange(frame: .zero, animationDuration: animationDuration)
        }

        return Observable.merge(keyboardWillShowFrames, keyboardWillHideFrames)
    }

}
