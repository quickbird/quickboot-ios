//
//  DateFormatter+Init.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import Foundation

extension DateFormatter {

    public convenience init(dateFormat: String) {
        self.init()
        self.dateFormat = dateFormat
    }

}
