//
//  UIColor+Hex.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit

extension UIColor {

    public convenience init?(hex: String, alpha: CGFloat = 1.0) {
        var rgbValue: UInt32 = 0
        var colorString = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if colorString.hasPrefix("#") {
            colorString.remove(at: colorString.startIndex)
        }
        
        Scanner(string: colorString).scanHexInt32(&rgbValue)

        self.init(red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
                  green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
                  blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
                  alpha: alpha)
    }

}
