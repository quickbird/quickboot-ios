//
//  UIColor+Modifications.swift
//  QuickBoot
//
//  Created by Stefan Kofler on 21.04.18.
//  Copyright © 2018 QuickBird Studios GmbH. All rights reserved.
//

import UIKit

extension UIColor {

    public func lighter(amount: CGFloat = 0.25) -> UIColor {
        return hueColorWithBrightnessAmount(1.0 + amount)
    }

    public func darker(amount: CGFloat = 0.25) -> UIColor {
        return hueColorWithBrightnessAmount(1.0 - amount)
    }

    public func mix(with color: UIColor, alpha: CGFloat) -> UIColor {
        let alpha2 = min(1.0, max(0, alpha))
        let beta = 1.0 - alpha2

        var r1: CGFloat = 0, r2: CGFloat = 0
        var g1: CGFloat = 0, g2: CGFloat = 0
        var b1: CGFloat = 0, b2: CGFloat = 0
        var a1: CGFloat = 0, a2: CGFloat = 0

        if getRed(&r1, green: &g1, blue: &b1, alpha: &a1) && color.getRed(&r2, green: &g2, blue: &b2, alpha: &a2) {
            let red = r1 * beta + r2 * alpha2
            let green = g1 * beta + g2 * alpha2
            let blue = b1 * beta + b2 * alpha2
            let alpha = a1 * beta + a2 * alpha2
            return UIColor(red: red, green: green, blue: blue, alpha: alpha)
        }

        return self
    }

    private func hueColorWithBrightnessAmount(_ amount: CGFloat) -> UIColor {
        var hue: CGFloat = 0
        var saturation: CGFloat = 0
        var brightness: CGFloat = 0
        var alpha: CGFloat = 0

        if getHue(&hue, saturation: &saturation, brightness: &brightness, alpha: &alpha) {
            return UIColor(hue: hue,
                            saturation: saturation,
                            brightness: brightness * amount,
                            alpha: alpha)
        } else {
            return self
        }
    }

}
