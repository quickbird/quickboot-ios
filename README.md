# QuickBoot

[![CI Status](https://img.shields.io/travis/grafele/QuickBoot.svg?style=flat)](https://travis-ci.org/grafele/QuickBoot)
[![Version](https://img.shields.io/cocoapods/v/QuickBoot.svg?style=flat)](https://cocoapods.org/pods/QuickBoot)
[![License](https://img.shields.io/cocoapods/l/QuickBoot.svg?style=flat)](https://cocoapods.org/pods/QuickBoot)
[![Platform](https://img.shields.io/cocoapods/p/QuickBoot.svg?style=flat)](https://cocoapods.org/pods/QuickBoot)

QuickBird Studios iOS Base Library

## Usage Alerts

To show alerts an AlertViewModel instance first. The geric parameter can be any class conforming to *CustomStringConvertible* even Strings themself. AlertItems can be constructed with the static methods for default, destructive and cancel buttons.

```swift
let viewModel = AlertViewModel(title: "Title", message: "Long message bla bla", items: [
    .default("Yes"),
    .cancel("No"),
])
```

If you want to be more type safe use for example an enum describing all the different button types.

```swift
enum AlertOption: CustomStringConvertible {
    case yes
    case no

    var description: String {
        switch self {
        case .yes:
            return "Yes"
        case .no:
            return "No"
        }
    }
}

let viewModel = AlertViewModel<AlertOption>(title: "Title", message: "Long message bla bla", items: [
    .default(.yes),
    .cancel(.no),
])
```

Afterwards create an instance of *AlertViewController* with it's preferred style and it's ViewModel.

```swift
let vc = AlertViewController(preferredStyle: .alert, viewModel: viewModel)
```

Finally you can present your view controller normally. If you want to observe the button clicks using the *selectedItem* output observable giving you the concrete item type.

```swift
viewModel.selectedItem.subscribe(onNext: { option in
    switch option {
    case .yes:
        print("Selected yes option")
    case .no:
        print("Selected no option")
    }
})
```

## Author

QuickBird Studios GmbH, contact@quickbirdstudios.com

## License

QuickBoot is available under the MIT license. See the LICENSE file for more info.
